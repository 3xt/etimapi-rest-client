# EtimApi REST Client

This EtimApi REST Client can be used to see the EtimApi REST services in action.

> Version v1.0 is deprecated. This version will stay active, but further development will take place on v2.0

## v2.0

- Great ways to search and filter the ETIM classifications.
- More consistent services and models
- Possibility to get a complete selection/release with one servicecall. Ability to use paging.

## v1.0

- Deprecated

## Prerequisites

- client_id/client_secret from ETIM International
- Installation of git
- Installation of Visual Studio Code
- Installation of the REST Client extension

## Getting Started

- Get a client_id/client_secret at [https://etimapi.etim-international.com/](https://etimapi.etim-international.com/)
- Download and install git [git](https://git-scm.com/downloads)
- Download and install Visual Studio Code. [https://code.visualstudio.com/download](https://code.visualstudio.com/download)
- Start Visual Studio Code. Search for and install extension [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
- Open a command prompt and clone this repository with git

## Settings
Some variables are used from the settings file. 
* Create a settings.json file in the directory .vscode --> ".vscode/settings.json"
* Copy this into the setting.json file:

```json
{
"rest-client.environmentVariables": {
	"production": {
		"authUrl": "https://etimauth.etim-international.com",
		"baseUrl": "https://etimapi.etim-international.com",
		"client_id": "",
		"client_secret": "",
		"scope": "EtimApi"
	}
}
}
```
* Fill in the client_id/client_secret you received from ETIM International

## Start you first request
* Open one of the .http files in Visual Studio Code
* Switch the environment in the Status bar to "production"
* Click on "Send Request" in the "ClientCredentialsGrant". Or place your cursor between ### and ### and use the keyboard shortcut ctrl-alt-r. This service will fetch an access-token that will be used in subsequential requests.
* Next click on "Send Request" above one of the service calls.
